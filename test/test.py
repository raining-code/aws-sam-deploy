import os
import shutil
import subprocess

import pytest
import boto3

from bitbucket_pipes_toolkit.test import PipeTestCase


AWS_DEFAULT_REGION = 'us-east-1'
AWS_SAM_TEST_STACK_NAME = f"sam-deploy-test-infra-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
S3_BUCKET = f"bbci-pipes-sam-deploy-test-infra-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
PIPE_ROOT_DIR = '/usr/bin/'


def delete_stack(stack_name, waiter_name):
    client = boto3.client(
        'cloudformation',
        region_name=os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION))

    waiter = client.get_waiter(waiter_name)

    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Waiter.StackUpdateComplete
    # default time to wait 30*120 sec
    # will wait fo 30*10 sec
    waiter_params = {
        'StackName': stack_name,
        'WaiterConfig': {
            'Delay': 30,
            'MaxAttempts': 10
        }
    }

    if waiter.wait(**waiter_params) is None:
        client.delete_stack(StackName=stack_name)


class SamDeployTestCase(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.aws_region = os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION)
        cls.aws_stack_name = f"{AWS_SAM_TEST_STACK_NAME}-{cls.aws_region}"
        cls.init_sam_app()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        shutil.rmtree('sam-app', ignore_errors=True)
        delete_stack(cls.aws_stack_name, waiter_name='stack_update_complete')

    @staticmethod
    def init_sam_app():
        args = ('sam init --name sam-app --runtime python3.7 --app-template hello-world '
                '--dependency-manager pip --no-interactive')
        subprocess.run(args.split())

    def test_fail_if_no_params(self):
        result = self.run_container()
        self.assertRegex(
            result, rf'✖ Validation errors')

    def test_create_successful(self):
        result = self.run_container(
            environment={
                'BITBUCKET_PIPE_STORAGE_DIR': PIPE_ROOT_DIR,
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'S3_BUCKET': S3_BUCKET,
                'CAPABILITIES_COUNT': 2,
                'CAPABILITIES_0': 'CAPABILITY_IAM',
                'CAPABILITIES_1': 'CAPABILITY_AUTO_EXPAND',
                'STACK_NAME': self.aws_stack_name,
                'SAM_TEMPLATE': 'sam-app/template.yaml',
                'WAIT': True,
            }
        )
        self.assertRegex(
            result, rf'✔ Successfully created the {self.aws_stack_name} stack')

    def test_packge_fail_if_template_not_exist(self):
        result = self.run_container(
            environment={
                'BITBUCKET_PIPE_STORAGE_DIR': PIPE_ROOT_DIR,
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'S3_BUCKET': S3_BUCKET,
                'CAPABILITIES_COUNT': 2,
                'CAPABILITIES_0': 'CAPABILITY_IAM',
                'CAPABILITIES_1': 'CAPABILITY_AUTO_EXPAND',
                'STACK_NAME': self.aws_stack_name,
                'SAM_TEMPLATE': 'template_not_exist.yaml',
                'WAIT': True,
            }
        )
        self.assertRegex(
            result, rf'✖ Failed SAM package')

    def test_packge_fail_if_s3_bucket_not_exist(self):
        result = self.run_container(
            environment={
                'BITBUCKET_PIPE_STORAGE_DIR': PIPE_ROOT_DIR,
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'S3_BUCKET': 'aws_s3_bucket_not_exist_001',
                'CAPABILITIES_COUNT': 2,
                'CAPABILITIES_0': 'CAPABILITY_IAM',
                'CAPABILITIES_1': 'CAPABILITY_AUTO_EXPAND',
                'STACK_NAME': self.aws_stack_name,
                'SAM_TEMPLATE': 'sam-app/template.yaml',
                'WAIT': True,
            }
        )
        self.assertRegex(
            result, rf'✖ Failed SAM package')

    def test_package_only_success(self):
        result = self.run_container(
            environment={
                'BITBUCKET_PIPE_STORAGE_DIR': PIPE_ROOT_DIR,
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'S3_BUCKET': S3_BUCKET,
                'CAPABILITIES_COUNT': 2,
                'CAPABILITIES_0': 'CAPABILITY_IAM',
                'CAPABILITIES_1': 'CAPABILITY_AUTO_EXPAND',
                'COMMAND': 'package-only',
                'STACK_NAME': self.aws_stack_name,
                'SAM_TEMPLATE': 'sam-app/template.yaml',
                'WAIT': True,
            }
        )
        self.assertIn('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.', result)
        self.assertRegex(
            result,
            rf'Packaged application uploaded to S3 bucket {S3_BUCKET} and generated CloudFormation template written to'
        )

    def test_oidc_successfull_all(self):
        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'S3_BUCKET': S3_BUCKET,
            'CAPABILITIES_COUNT': 2,
            'CAPABILITIES_0': 'CAPABILITY_IAM',
            'CAPABILITIES_1': 'CAPABILITY_AUTO_EXPAND',
            'STACK_NAME': self.aws_stack_name,
            'SAM_TEMPLATE': 'sam-app/template.yaml',
            'BITBUCKET_PIPE_STORAGE_DIR': PIPE_ROOT_DIR,
            'WAIT': True,
        })
        self.assertIn('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.', result)
        self.assertRegex(
            result,
            rf'✔ Successfully updated the {self.aws_stack_name} stack')

    def test_package_with_non_existing_sam_config(self):
        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'COMMAND': 'package-only',
            'S3_BUCKET': S3_BUCKET,
            'CAPABILITIES_COUNT': 2,
            'CAPABILITIES_0': 'CAPABILITY_IAM',
            'CAPABILITIES_1': 'CAPABILITY_AUTO_EXPAND',
            'SAM_TEMPLATE': 'template.yaml',
            'BITBUCKET_PIPE_STORAGE_DIR': PIPE_ROOT_DIR,
            'SAM_CONFIG': 'non-existing.toml',
            'WAIT': True,
        })
        self.assertRegex(
            result,
            rf'Packaging has failed. Passed SAM_CONFIG path does not exist'
        )

    @pytest.mark.run(order=-2)
    def test_package_with_sam_config(self):
        sam_prefix_folder = 'config-s3-prefix'
        sam_config_template = f"""
        version=0.1
        [default.package.parameters]
        s3_prefix="{sam_prefix_folder}"
        """
        test_dir = os.getcwd()
        os.chdir('sam-app')
        sam_config_file = 'config.toml'
        with open(sam_config_file, 'w') as f:
            f.write(sam_config_template)

        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'COMMAND': 'package-only',
            'S3_BUCKET': S3_BUCKET,
            'SAM_TEMPLATE': 'template.yaml',
            'BITBUCKET_PIPE_STORAGE_DIR': PIPE_ROOT_DIR,
            'SAM_CONFIG': sam_config_file,
            'WAIT': True,
            'DEBUG': True
        })
        self.assertIn('Sending Telemetry: ', result)
        self.assertIn('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.', result)
        self.assertRegex(
            result,
            rf'Packaged application uploaded to S3 bucket {S3_BUCKET} and generated CloudFormation template written to'
        )
        s3 = boto3.client('s3', region_name=os.getenv('AWS_DEFAULT_REGION'))
        content_packaged = s3.list_objects(Bucket=S3_BUCKET, Prefix=sam_prefix_folder)
        self.assertEqual(1, len(content_packaged['Contents']))
        self.assertRegex(content_packaged['Contents'][0]['Key'], rf'{sam_prefix_folder}\/[a-zA-Z0-9]*$')
        # change back to previous directory
        os.remove(sam_config_file)
        os.chdir(test_dir)

    @pytest.mark.run(order=-1)
    def test_deploy_only_success(self):
        result = self.run_container(
            environment={
                'BITBUCKET_PIPE_STORAGE_DIR': PIPE_ROOT_DIR,
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'S3_BUCKET': S3_BUCKET,
                'CAPABILITIES_COUNT': 2,
                'CAPABILITIES_0': 'CAPABILITY_IAM',
                'CAPABILITIES_1': 'CAPABILITY_AUTO_EXPAND',
                'COMMAND': 'deploy-only',
                'STACK_NAME': self.aws_stack_name,
                'TEMPLATE': 'https://sam-deploy-pipe.s3.amazonaws.com/packaged.yaml',
                'WAIT': True,
            }
        )
        self.assertRegex(
            result, rf'✔ Successfully updated the {self.aws_stack_name} stack')
