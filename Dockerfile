FROM python:3.7

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN wget --no-verbose https://github.com/aws/aws-sam-cli/releases/download/v1.36.0/aws-sam-cli-linux-x86_64.zip && \
       echo '48866227639fb8eda1b4f5445fe7f7e99f006a7a908cc1744dd21dc0e6442a3e aws-sam-cli-linux-x86_64.zip' | sha256sum -c - && \
       unzip aws-sam-cli-linux-x86_64.zip -d sam-installation && ./sam-installation/install

COPY requirements.txt /usr/bin

WORKDIR /usr/bin

RUN pip install --no-cache-dir -r requirements.txt

COPY pipe.yml /usr/bin
COPY sam_pipe /usr/bin/

ENTRYPOINT ["python3", "/usr/bin/core.py"]
